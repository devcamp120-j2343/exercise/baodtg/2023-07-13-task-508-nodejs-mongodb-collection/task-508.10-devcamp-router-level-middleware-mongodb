//B1: khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose;
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]
    

});
//B4: export schema ra model
module.exports = mongoose.model('course', courseSchema)